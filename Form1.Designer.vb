﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGetInt = New System.Windows.Forms.Button()
        Me.btnGetLong = New System.Windows.Forms.Button()
        Me.btnGetString = New System.Windows.Forms.Button()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnRandom = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnGetInt
        '
        Me.btnGetInt.Location = New System.Drawing.Point(52, 32)
        Me.btnGetInt.Name = "btnGetInt"
        Me.btnGetInt.Size = New System.Drawing.Size(75, 23)
        Me.btnGetInt.TabIndex = 0
        Me.btnGetInt.Text = "Get Int"
        Me.btnGetInt.UseVisualStyleBackColor = True
        '
        'btnGetLong
        '
        Me.btnGetLong.Location = New System.Drawing.Point(52, 61)
        Me.btnGetLong.Name = "btnGetLong"
        Me.btnGetLong.Size = New System.Drawing.Size(75, 23)
        Me.btnGetLong.TabIndex = 1
        Me.btnGetLong.Text = "Get Long"
        Me.btnGetLong.UseVisualStyleBackColor = True
        '
        'btnGetString
        '
        Me.btnGetString.Location = New System.Drawing.Point(52, 90)
        Me.btnGetString.Name = "btnGetString"
        Me.btnGetString.Size = New System.Drawing.Size(75, 23)
        Me.btnGetString.TabIndex = 2
        Me.btnGetString.Text = "Get String"
        Me.btnGetString.UseVisualStyleBackColor = True
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(25, 148)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ReadOnly = True
        Me.txtOutput.Size = New System.Drawing.Size(135, 67)
        Me.txtOutput.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(85, 221)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnRandom
        '
        Me.btnRandom.Location = New System.Drawing.Point(52, 119)
        Me.btnRandom.Name = "btnRandom"
        Me.btnRandom.Size = New System.Drawing.Size(75, 23)
        Me.btnRandom.TabIndex = 5
        Me.btnRandom.Text = "Random"
        Me.btnRandom.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(187, 254)
        Me.Controls.Add(Me.btnRandom)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.btnGetString)
        Me.Controls.Add(Me.btnGetLong)
        Me.Controls.Add(Me.btnGetInt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JNBridge Test"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGetInt As System.Windows.Forms.Button
    Friend WithEvents btnGetLong As System.Windows.Forms.Button
    Friend WithEvents btnGetString As System.Windows.Forms.Button
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnRandom As System.Windows.Forms.Button

End Class
