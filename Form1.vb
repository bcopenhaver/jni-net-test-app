﻿Imports com.turningtech.jnitest
Imports com.turningtech.wrappers

Public Class Form1

    Private WithEvents javaProcess As Process
    Private _jniTestObject As JniTest
    Private _StringGeneratedEventHandler As StringGeneratedEventHandler

    Private Sub Form1_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        javaProcess = New Process
        javaProcess.StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
        javaProcess.StartInfo.FileName = "java.exe"
        javaProcess.StartInfo.Arguments = "-cp "".;jnbcore.jar;jni-test-library-1.0.0-SNAPSHOT.jar"" com.jnbridge.jnbcore.JNBMain /props ""jnbcore_tcp.properties"""
        javaProcess.StartInfo.UseShellExecute = False
        javaProcess.StartInfo.CreateNoWindow = True
        javaProcess.Start()
        _jniTestObject = New JniTest("JNBridge Test API", 100, 123456789)
        _StringGeneratedEventHandler = New StringGeneratedEventHandler
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If javaProcess.HasExited = False Then
            Try
                javaProcess.Kill()
            Catch ex As Exception
            End Try
        End If
        Try
            If Not javaProcess Is Nothing Then
                javaProcess.Dispose()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnGetInt_Click(sender As System.Object, e As System.EventArgs) Handles btnGetInt.Click
        Dim val As Integer
        val = _jniTestObject.getIntValue()
        txtOutput.Text = val.ToString
    End Sub

    Private Sub btnGetLong_Click(sender As System.Object, e As System.EventArgs) Handles btnGetLong.Click
        Dim val As Long
        val = _jniTestObject.getLongValue
        txtOutput.Text = val.ToString
    End Sub

    Private Sub btnGetString_Click(sender As System.Object, e As System.EventArgs) Handles btnGetString.Click
        Dim val As String
        val = _jniTestObject.getStringValue
        txtOutput.Text = val
    End Sub

    Private Sub btnRandom_Click(sender As System.Object, e As System.EventArgs) Handles btnRandom.Click
        Try
            _jniTestObject.generateRandomString(_StringGeneratedEventHandler)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click

        If javaProcess.HasExited = False Then
            Try
                javaProcess.Kill()
            Catch ex As Exception
            End Try
        End If
        Me.Close()
    End Sub

    Private Sub UpdateRandomString(ByRef message As String)
        txtOutput.Text = message
    End Sub

    <com.jnbridge.jnbcore.AsyncCallback("com.turningtech.jnitest.EventInterface")> _
    Public Class StringGeneratedEventHandler
        Implements com.turningtech.jnitest.EventInterface

        Public Sub stringGeneratedEvent(p1 As String) Implements com.turningtech.jnitest.EventInterface.stringGeneratedEvent
            MsgBox("Success")
        End Sub

    End Class

End Class
